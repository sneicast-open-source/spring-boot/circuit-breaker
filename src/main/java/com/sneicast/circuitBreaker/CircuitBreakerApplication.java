package com.sneicast.circuitBreaker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class CircuitBreakerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CircuitBreakerApplication.class, args);
	}
	
	/*@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}*/
	@Bean
	  public RestTemplate rest(RestTemplateBuilder builder) {
	    return builder.build();
	  }

}
