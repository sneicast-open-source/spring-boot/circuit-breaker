package com.sneicast.circuitBreaker.countries.services;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class CountriesServiceImpl implements ICountriesService {
	//private final RestTemplate restTemplate;
	
	@Autowired
	private final RestTemplate apiRestClient;
	
	@Value("${url.api}")
	private String apiUrl;
	
	
	public CountriesServiceImpl(RestTemplate restTemplate) {
	    this.apiRestClient = restTemplate;
	  }
	
	@Override
	@HystrixCommand(fallbackMethod = "defaultResponse")
	public String GetAll() {
		 // return "Error Al consumir servicio Externo";
		HttpEntity<String> customHeaders = new HttpEntity<String>("parameters");
		String resp = apiRestClient.exchange(apiUrl, HttpMethod.GET, customHeaders, String.class).getBody();
		return resp;
	}
	


	  public String defaultResponse() {
	    return "Error Al consumir servicio Externo";
	  }

}
