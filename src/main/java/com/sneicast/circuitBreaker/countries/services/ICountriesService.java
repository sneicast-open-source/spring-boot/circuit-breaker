package com.sneicast.circuitBreaker.countries.services;

import org.springframework.web.client.RestTemplate;

public interface ICountriesService {

	String GetAll();
}
