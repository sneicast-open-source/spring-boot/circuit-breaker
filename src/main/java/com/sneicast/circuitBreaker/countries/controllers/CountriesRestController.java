package com.sneicast.circuitBreaker.countries.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sneicast.circuitBreaker.countries.services.ICountriesService;

@RestController
@RequestMapping(path = "/api/v1")
@CrossOrigin("*")
@EnableCircuitBreaker
public class CountriesRestController {
	@Autowired
	private ICountriesService countriesServie;
	
	//@GetMapping("/costumer-status/{issuedIdentType}/{issuedIdentValue}")
	@GetMapping("/countries")
	public ResponseEntity<String> getAll() {
		try {
		String countries = countriesServie.GetAll();
		return new ResponseEntity<>(countries, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>("Gran Error", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
