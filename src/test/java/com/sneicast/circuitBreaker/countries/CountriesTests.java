package com.sneicast.circuitBreaker.countries;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import com.sneicast.circuitBreaker.countries.controllers.CountriesRestController;
import com.sneicast.circuitBreaker.countries.services.CountriesServiceImpl;
import com.sneicast.circuitBreaker.countries.services.ICountriesService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CountriesTests {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ICountriesService countriesService;

	@Test
	public void countriesTestIsOk() throws Exception {
		//assertEquals("Error Al consumir servicio Externo", countriesService.GetAll());
		
		given(countriesService.GetAll()).willReturn("Error Al consumir servicio Externo");
		// when + then

		this.mockMvc.perform(get("/api/v1/countries")).andExpect(status().isOk())
				.andExpect(content().string("Error Al consumir servicio Externo"));

	}
	/*@Test
	public void countriesTestIsFail() throws Exception {
		//assertEquals("Error Al consumir servicio Externo", countriesService.GetAll());
		
		given(countriesService.GetAll()).willReturn("Error Al consumir servicio Externo");
		// when + then

		//this.mockMvc.perform(get("/api/v1/countries")).andExpect(status().isInternalServerError())
			//	.andExpect(content().string("Gran Error"));

		 when(this.mockMvc.perform(get("/api/v1/countries")))
	      .thenThrow(NullPointerException.class);
	}*/
	
	
	//Controlador
	
	/* @InjectMocks
	    private CountriesRestController countriesController;
	 @Test
	    public void handle_exception_to_general_error_code() {
	        
	    }*/

}
